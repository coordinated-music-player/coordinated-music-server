package models

import models.Agent.PlayState
import models.Agent.PlayState.PlayState

class Agent(val name: String, var queueName: Option[String]) {
  def setQueueName(queue: String): Unit = queueName = Option(queue)
  def removeQueue(): Unit = queueName = None
  var playState: PlayState = PlayState.STOP
  var _playPositionSeconds: Double = 0
  var createTime = System.currentTimeMillis()
  var cacheState: List[(String, Double)] = Nil

  def playPositionSeconds: Double = {
    if (playState == PlayState.PLAY) {
      val elapsed = System.currentTimeMillis() - createTime
      _playPositionSeconds + elapsed.toDouble / 1000
    } else {
      _playPositionSeconds
    }
  }

  def playPositionSeconds_=(v: Double): Unit = {
    _playPositionSeconds = v
    createTime = System.currentTimeMillis()
  }

  def printablePlayState: String = {
    playState match {
      case PlayState.PLAY => {
        "play"
      }
      case PlayState.PAUSE => {
        "pause"
      }
      case PlayState.STOP => {
        "stop"
      }
    }
  }

  def setPlayStateFromString(ps: String): Unit = {
    ps match {
      case "play" => {
        playState = PlayState.PLAY
      }
      case "pause" => {
        playState = PlayState.PAUSE
      }
      case "stop" => {
        playState = PlayState.STOP
      }
      case _ => {
        playState = PlayState.STOP
      }
    }
  }
}

object Agent {
  object PlayState extends Enumeration {
    type PlayState = Value
    val PLAY, PAUSE, STOP = Value
  }
}
