package models

import play.api.libs.json.JsArray
import play.api.libs.json.JsValue
import service.DatabaseService

class PlayQueue(val id: String) {
  var queue: List[QueueItem] = List()
  var seconds: Double = 0.0
  var song: Int = 0

  def mapFrom(_others: List[QueueItem]): Unit = {
    var others = _others
    queue.foreach(qi => {
      val from = others.find(_.sha1 == qi.sha1)
      var isRemoved = false
      others = others.filter(i => {
        if (!isRemoved && i.sha1 == qi.sha1) {
          isRemoved = true
          false
        } else {
          true
        }
      })
      from.foreach(f => {
        println(s"Updating ${qi} to ${f.secondsPosition}")
        qi.secondsPosition = f.secondsPosition
      })
    })
  }
}

object PlayQueue {
  def fromJson(js: JsValue, db: DatabaseService): Option[PlayQueue] = {
    val name: Option[String] = js.\("id").asOpt[String]
    val queue: Option[JsArray] = js.\("queue").asOpt[JsArray]
    val seconds: Option[Double] = js.\("seconds").asOpt[Double]
    val song: Option[Int] = js.\("song").asOpt[Int]
    name.map({ n =>
      val pq = new PlayQueue(n)
      pq.queue = queue.map(lst => lst.value.toList.flatMap(qi => QueueItem.fromJson(qi, db))).getOrElse(List())
      seconds.foreach(pq.seconds = _)
      song.foreach(pq.song = _)
      pq
    })
  }
}