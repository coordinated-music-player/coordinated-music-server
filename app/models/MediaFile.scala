package models

import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.security.DigestInputStream

import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.audio.mp3.MP3File
import org.jaudiotagger.tag.FieldKey
import org.jaudiotagger.tag.KeyNotFoundException
import org.jaudiotagger.tag.flac.FlacTag
import org.jaudiotagger.tag.id3.ID3v24Frames
import org.jaudiotagger.tag.vorbiscomment.VorbisCommentTag
import play.api.libs.json.JsNumber
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import service.DatabaseService

class MediaFile(val parentPath: String, val slug: String, val path: File) {
  var _meta: MediaFileMeta = null
  var _sha1: String = null

  def sha1: String = {
    if (_sha1 == null) {
      val md = java.security.MessageDigest.getInstance("SHA-1")
      val stream = new BufferedInputStream(new FileInputStream(path))
      val mds = new DigestInputStream(stream, md)
      while (mds.read() != -1) {}
      _sha1 = md.digest().map("%02x".format(_)).mkString
      stream.close()
    }
    _sha1
  }

  def meta: MediaFileMeta = {
    if (_meta == null) {
      _meta = try {
        if (path.getAbsolutePath.endsWith(".mp3")) {
          val f: MP3File = AudioFileIO.read(path).asInstanceOf[MP3File]
          val durationSeconds = try { f.getAudioHeader.getTrackLength } catch { case _ => { 0 } }
          val tag1 = f.getID3v1Tag
          val tag20 = f.getID3v2Tag
          val tag2 = f.getID3v2TagAsv24
          val title: String = {
            val v1 = if (tag1 != null) { tag1.getFirstTitle } else { null }
            val v2 = if (tag2 != null) { tag2.getFirst(ID3v24Frames.FRAME_ID_TITLE) } else { null }
            println(s"v1: ${v1}, v2: ${v2}")
            if (v2 != null && !v2.isEmpty) {
              v2
            } else if (v1 != null && !v1.isEmpty) {
              v1
            } else {
              path.getName
            }
          }
          println(s"title: ${title}")
          val artist: Option[String] = {
            val v1 = if (tag1 != null) { tag1.getFirstArtist } else { null }
            val v2 = if (tag2 != null) { tag2.getFirst(ID3v24Frames.FRAME_ID_ARTIST) } else { null }
            if (v2 != null && !v2.isEmpty) {
              Option(v2)
            } else if (v1 != null && !v1.isEmpty) {
              Option(v1)
            } else {
              None
            }
          }
          val album: String = {
            val v1 = if (tag1 != null) { tag1.getFirstAlbum } else { null }
            val v2 = if (tag2 != null) { tag2.getFirst(ID3v24Frames.FRAME_ID_ALBUM) } else { null }
            if (v2 != null && !v2.isEmpty) {
              v2
            } else if (v1 != null && !v1.isEmpty) {
              v1
            } else {
              path.getParentFile.getName
            }
          }
          val trackS: String = {
            val v1 = try { if (tag1 != null) { tag1.getFirstTrack } else { null } } catch { case _ => null }
            val v2 = if (tag2 != null) { tag2.getFirst(ID3v24Frames.FRAME_ID_TRACK) } else { null }
            if (v2 != null && !v2.isEmpty) {
              v2
            } else if (v1 != null && !v1.isEmpty) {
              v1
            } else {
              "0"
            }
          }
          val track: Int = try {
            trackS.toInt
          } catch {
            case _ => 0
          }
          val yearS: String = {
            val v1 = if (tag1 != null) { tag1.getFirstYear } else { null }
            val v2 = if (tag2 != null) { tag2.getFirst(ID3v24Frames.FRAME_ID_YEAR) } else { null }
            if (v2 != null && !v2.isEmpty) {
              v2
            } else if (v1 != null && !v1.isEmpty) {
              v1
            } else {
              "0"
            }
          }
          val year: Int = try {
            yearS.toInt
          } catch {
            case _ => 0
          }
          MediaFileMeta(
            title = title,
            artist = artist,
            album = album,
            track = track,
            year = year,
            durationSeconds = durationSeconds
          )
        } else {
          val f = AudioFileIO.read(path)
          val durationSeconds = try { f.getAudioHeader.getTrackLength } catch { case _ => { 0 } }
          val meta = try {
            Option(f.getTag)
          } catch {
            case _ => None
          }
          meta match {
            case Some(tag: VorbisCommentTag) => {
              val title = try { tag.getFirst(FieldKey.TITLE) } catch { case e: KeyNotFoundException => { path.getName } }
              val artist = try { Option(tag.getFirst(FieldKey.ARTIST)) } catch { case e: KeyNotFoundException => { None } }
              val album = try { tag.getFirst(FieldKey.ALBUM) } catch { case e: KeyNotFoundException => { path.getParentFile.getName } }
              val year = try { tag.getFirst(FieldKey.YEAR).toInt } catch { case _ => { 0 } }
              val track = try { tag.getFirst(FieldKey.TRACK).toInt } catch { case _ => { 0 } }
              MediaFileMeta(
                title = title,
                artist = artist,
                album = album,
                track = track,
                year = year,
                durationSeconds = durationSeconds
              )
            }
            case Some(tag: FlacTag) => {
              val title = try { tag.getFirst(FieldKey.TITLE) } catch { case e: KeyNotFoundException => { path.getName } }
              val artist = try { Option(tag.getFirst(FieldKey.ARTIST)) } catch { case e: KeyNotFoundException => { None } }
              val album = try { tag.getFirst(FieldKey.ALBUM) } catch { case e: KeyNotFoundException => { path.getParentFile.getName } }
              val year = try { tag.getFirst(FieldKey.YEAR).toInt } catch { case _ => { 0 } }
              val track = try { tag.getFirst(FieldKey.TRACK).toInt } catch { case _ => { 0 } }
              MediaFileMeta(
                title = title,
                artist = artist,
                album = album,
                track = track,
                year = year,
                durationSeconds = durationSeconds
              )
            }
            case _ => {
              MediaFileMeta(path.getName, None, path.getParentFile.getName, 0, 0, 0)
            }
          }
        }
      } catch {
        case e => {
          e.printStackTrace()
          println(s"Caught exception, returning default meta: ${e}")
          MediaFileMeta(
            title = path.getName,
            artist = None,
            album = path.getParentFile.getName,
            track = 0,
            year = 0,
            durationSeconds = 0
          )
        }
      }
    }
    _meta
  }

  def index(db: DatabaseService): Unit = {
    println(s"Indexing ${parentPath}/${path.getName}")
    db.indexMedia(this)
  }

  def toJson: JsValue = {
    JsObject(List(
      "parent" -> JsString(parentPath),
      "path" -> JsString(path.getName),
      "slug" -> JsString(slug),
      "title" -> JsString(meta.title),
      "artist" -> JsString(meta.artist.getOrElse("")),
      "album" -> JsString(meta.album),
      "track" -> JsNumber(meta.track),
      "year" -> JsNumber(meta.year),
      "durationSeconds" -> JsNumber(meta.durationSeconds)
    ))
  }
}

case class MediaFileMeta(
                        title: String,
                        artist: Option[String],
                        album: String,
                        track: Int,
                        year: Int,
                        durationSeconds: Int
                        )
