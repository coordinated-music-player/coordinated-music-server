package models

import java.io.File

import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import service.DatabaseService

class MediaFolder(val parentPath: String, val name: String, val path: File, extensions: Seq[String]) {
  def slug: String = {
    parentPath + "/" + name
  }

  def getSubFolders: Seq[MediaFolder] = {
    if (path != null && path.listFiles() != null) {
      path.listFiles().toSeq.filter({ sub =>
        sub.isDirectory
      }).map({ sub =>
        new MediaFolder(slug, sub.getName, sub, extensions)
      }).sortBy(_.path)
    } else {
      println(s"Couldn't get subfolders for ${path}")
      List()
    }
  }

  def getMusicFiles: Seq[MediaFile] = {
    if (path != null && path.listFiles() != null) {
      path.listFiles().toSeq.filter({ sub =>
        sub.isFile && extensions.exists({ ext =>
          sub.getName.trim.toLowerCase.endsWith(ext.toLowerCase)
        })
      }).map({ sub =>
        new MediaFile(slug, s"${slug}/${sub.getName}", sub)
      }).sortBy(_.path)
    } else {
      List()
    }
  }

  def toJson(depth: Int, db: DatabaseService): JsValue = {
    if (depth > 0) {
      JsObject(List(
        "path" -> JsString(name),
        "slug" -> JsString(slug),
        "children" -> JsArray(
          getSubFolders.map(_.toJson(depth - 1, db))
        ),
        "music" -> JsArray(
          db.getMedia(this).map(_.toJson)
        )
      ))
    } else {
      JsObject(List(
        "path" -> JsString(name),
        "slug" -> JsString(slug)
      ))
    }
  }

  def index(depth: Int, db: DatabaseService): Unit = {
    println(s"Indexing ${slug}")
    getMusicFiles.foreach(db.indexMedia)
    if (depth > 0) {
      getSubFolders.foreach(_.index(depth - 1, db))
    }
  }

  def findFolder(search: Seq[String]): Option[MediaFolder] = {
    if (search.head.equalsIgnoreCase(path.getName)) {
      if (search.tail == Nil) {
        Option(this)
      } else {
        getSubFolders.foldLeft[Option[MediaFolder]](None)({ (found, next) =>
          Option(found.getOrElse(next.findFolder(search.tail).orNull))
        })
      }
    } else {
      None
    }
  }

  def find(path: Seq[String]): Option[MediaFile] = {
    println(s"looking in ${this.slug}")
    if (path.tail == Nil) {
      getMusicFiles.foldLeft[Option[MediaFile]](None)({ (found, next) =>
        Option(found.getOrElse[MediaFile](
          if (next.path.getName.equalsIgnoreCase(path.head)) {
            next
          } else {
            null
          }
        ))
      })
    } else {
      getSubFolders.foldLeft[Option[MediaFile]](None)({ (found, next) =>
        Option(found.getOrElse[MediaFile](
          if (next.name.equalsIgnoreCase(path.head)) {
            next.find(path.tail).orNull
          } else {
            null
          }
        ))
      })
    }
  }
}
