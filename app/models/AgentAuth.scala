package models

import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.mvc.Results
import service.DatabaseService

case class AgentAuth(name: String, token: String, status: AgentAuth.Status.Status, lastLogin: String) {
  def toSecureJson(): JsValue = {
    JsObject(List(
      "name" -> JsString(name),
      "status" -> JsString(AgentAuth.Status.toString(status)),
      "lastLogin" -> JsString(lastLogin)
    ))
  }

  def toFullJson(): JsValue = {
    JsObject(List(
      "name" -> JsString(name),
      "token" -> JsString(token),
      "status" -> JsString(AgentAuth.Status.toString(status)),
      "lastLogin" -> JsString(lastLogin)
    ))
  }
}

object AgentAuth {
  object Status extends Enumeration {
    type Status = Value
    val Pending, Approved, Rejected = Value

    def fromString(s: String): Option[Status] = {
      s match {
        case "Pending" => Some(Pending)
        case "Approved" => Some(Approved)
        case "Rejected" => Some(Rejected)
        case _ => None
      }
    }

    def toString(s: Status): String = {
      s match {
        case Pending => "Pending"
        case Approved => "Approved"
        case Rejected => "Rejected"
      }
    }
  }

  def checkAuth(token: Option[String], db: DatabaseService)(f: (AgentAuth) => Results.Status): Results.Status = {
    token.flatMap({ t: String => db.getAuthApprovedByToken(t) }) match {
      case Some(auth) => {
        f(auth)
      }
      case None => Results.Unauthorized
    }
  }
}