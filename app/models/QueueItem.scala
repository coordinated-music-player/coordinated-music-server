package models

import play.api.libs.json.JsValue
import service.ConfigService
import service.DatabaseService

class QueueItem(val path: String,
                val title: String,
                val album: String,
                val artist: Option[String],
                val secondsDuration: Int,
                val track: Int,
                val year: Int,
                var secondsPosition: Int,
                val sha1: String
               ) {}

object QueueItem {
  def fromMediaFile(file: MediaFile): QueueItem = {
    new QueueItem(
      file.slug,
      file.meta.title,
      file.meta.album,
      file.meta.artist,
      file.meta.durationSeconds,
      file.meta.track,
      file.meta.year,
      0,
      file.sha1
    )
  }

  def create(path: String, configuration: ConfigService): Option[QueueItem] = {
    val media: Option[MediaFile] = configuration.findMediaFile(path)
    println(s"Searching for ${path}, Found media file: ${media}")
    media.map({ file => new QueueItem(
      path,
      file.meta.title,
      file.meta.album,
      file.meta.artist,
      file.meta.durationSeconds,
      file.meta.track,
      file.meta.year,
      0,
      file.sha1
    ) })
  }

  def fromJson(js: JsValue, db: DatabaseService): Option[QueueItem] = {
    js.\("path").asOpt[String].map({path =>
      val seconds: Option[Double] = js.\("secondsDuration").asOpt[Double]
      // TODO: Remove extra DB query after the queues have been converted
      val sha1 = js.\("sha1").asOpt[String].getOrElse({
        val path: Option[String] = js.\("path").asOpt[String]
        val media: Option[MediaFile] = path.flatMap(db.getDeepMedia(_).headOption)
        media.map(_.sha1).getOrElse("")
      })
      new QueueItem(
        path,
        js.\("title").asOpt[String].getOrElse(""),
        js.\("album").asOpt[String].getOrElse(""),
        js.\("artist").asOpt[String],
        js.\("secondsDuration").asOpt[Int].getOrElse(0),
        js.\("track").asOpt[Int].getOrElse(0),
        js.\("year").asOpt[Int].getOrElse(0),
        js.\("secondsPosition").asOpt[Int].getOrElse(0),
        sha1
      )
    })
  }

  def convertStringToDouble(s: String): Option[Double] = {
    try {
      Some(s.toDouble)
    } catch {
      case e: Exception => None
    }
  }
}
