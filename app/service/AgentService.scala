package service

import javax.inject.Inject

import controllers.AgentWebSocketActor
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import javax.inject.Singleton

import models.Agent
import play.api.libs.json.JsNumber

@Singleton
class AgentService @Inject() (interfaces: InterfaceService) {
  var agents: List[AgentWebSocketActor] = List()

  def add(agent: AgentWebSocketActor): Unit = {
    agents.find(a => a.state.map(_.name).getOrElse("") == agent.state.map(_.name).getOrElse("_")).foreach(_.kill())
    agents = agent :: agents
    println(s"Adding an agent, notify all ${interfaces.interfaces.size} interfaces")
    interfaces.interfaces.foreach({_.sendAgentsUpdate(List(agent), Nil, agents)})
  }

  def remove(agent: AgentWebSocketActor): Unit = {
    agents = agents.filter({ _ != agent })
    interfaces.interfaces.foreach({_.sendAgentsUpdate(Nil, List(agent), agents)})
  }

  def find(name: String): Option[AgentWebSocketActor] = {
    agents.find({ sock =>
      sock.state.isDefined && sock.state.get.name == name
    })
  }
}

object AgentService {
  val COMMAND_GIVE_UP: JsValue = JsObject(List("command" -> JsString("giveUp")))
  val COMMAND_PLAY: JsValue = JsObject(List("command" -> JsString("play")))
  val COMMAND_NEXT: JsValue = JsObject(List("command" -> JsString("next")))
  val COMMAND_PREV: JsValue = JsObject(List("command" -> JsString("prev")))
  val COMMAND_PAUSE: JsValue = JsObject(List("command" -> JsString("pause")))
  val COMMAND_STOP: JsValue = JsObject(List("command" -> JsString("stop")))
  def COMMAND_JUMP(songPosition: Int): JsValue = {
    JsObject(List("command" -> JsString("jump"), "songPosition" -> JsNumber(songPosition)))
  }
  def COMMAND_SEEK(seconds: Double): JsValue = {
    JsObject(List("command" -> JsString("seek"), "seconds" -> JsNumber(seconds)))
  }
  def COMMAND_FULL_QUEUE(agent: Agent, database: DatabaseService): Option[JsValue] = {
    agent.queueName.flatMap(database.getQueueJson(_).map({ q =>
      JsObject(List(
        "command" -> JsString("queue"),
        "queue" -> q
      ))
    }))
  }
  val COMMAND_DROP_QUEUE: JsValue = JsObject(List("command" -> JsString("dropQueue")))
  def COMMAND_INFO(msg: String): JsValue = {
    JsObject(List("command" -> JsString("info"), "message" -> JsString(msg)))
  }
  def COMMAND_ERROR(msg: String): JsValue = {
    JsObject(List("command" -> JsString("error"), "message" -> JsString(msg)))
  }
}
