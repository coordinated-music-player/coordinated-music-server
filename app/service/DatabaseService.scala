package service

import java.io.File
import java.security.MessageDigest
import java.util
import java.util.Base64
import java.util.NoSuchElementException
import javax.inject.Inject
import javax.inject.Singleton

import com.rethinkdb.RethinkDB
import com.rethinkdb.gen.ast.ReqlExpr
import com.rethinkdb.gen.ast.ReqlFunction1
import com.rethinkdb.net.Cursor
import models.Agent
import models.AgentAuth
import models.MediaFile
import models.MediaFileMeta
import models.MediaFolder
import models.PlayQueue
import models.QueueItem
import play.api.libs.json.JsArray
import play.api.libs.json.JsNumber
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.Json

@Singleton
class DatabaseService @Inject() (configuration: ConfigService) {
  private val r = RethinkDB.r
  val connectionBuilder = r.connection().hostname(configuration.rethinkIp)

  def indexMedia(media: MediaFile): Unit = {
    val slugSteps: Array[String] = {
      var combined = ""
      media.slug.split("/").flatMap({ part =>
        if (!part.isEmpty) {
          combined = s"${combined}/${part}"
          Option(combined)
        } else {
          None
        }
      })
    }
    val conn = connectionBuilder.connect()
    val result: java.util.HashMap[String, _] = r.db("music").table("media").insert(
      r.hashMap("id", sha1(media.slug))
        .`with`("fsPath", media.path.getAbsolutePath)
        .`with`("parentPath", media.parentPath)
        .`with`("filename", media.path.getName)
        .`with`("slug", media.slug)
        .`with`("title", media.meta.title)
        .`with`("artist", media.meta.artist.getOrElse(""))
        .`with`("album", media.meta.album)
        .`with`("track", media.meta.track)
        .`with`("year", media.meta.year)
        .`with`("sha1", media.sha1)
        .`with`("slugSteps", slugSteps)
        .`with`("durationSeconds", media.meta.durationSeconds)
    )
      .optArg("conflict", "update")
      .run(conn)
    conn.close()
    println(s"Saving ${media.slug} :: ${result}")
  }

  def getMedia(folder: MediaFolder): List[MediaFile] = {
    val conn = connectionBuilder.connect()
    val result: util.ArrayList[util.HashMap[String, _]] = r.db("music").table("media")
      .filter(r.hashMap("parentPath", folder.slug))
      .orderBy("track")
      .run(conn)
    parseMedia(result)
  }

  def getDeepMedia(slugPath: String): List[MediaFile] = {
    val escaped = s"/${slugPath.stripPrefix("/").stripSuffix("/")}"
    val conn = connectionBuilder.connect()
    val result: util.ArrayList[util.HashMap[String, _]] = r.db("music").table("media")
        .filter(new ReqlFunction1 {
          override def apply(doc: ReqlExpr): AnyRef = {
            val ret = doc.g("slugSteps").contains(escaped)
            ret
          }
        })
      .orderBy("album", "track", "title")
      .run(conn)
    parseMedia(result)
  }

  def parseMedia(result: util.ArrayList[util.HashMap[String, _]]): List[MediaFile] = {
    import scala.collection.JavaConversions._
    result.iterator().map({ r =>
      val album = r.get("album").toString
      val artist = r.get("artist").toString
      val fsPath = r.get("fsPath").toString
      val slug = r.get("slug").toString
      val _ = r.get("id").toString
      val parentPath = r.get("parentPath").toString
      val sha1 = r.get("sha1").toString
      val title = r.get("title").toString
      val track = r.get("track").asInstanceOf[Long].toInt
      val year = r.get("year").asInstanceOf[Long].toInt
      val durationSeconds = r.get("durationSeconds").asInstanceOf[Long].toInt
      val mf = new MediaFile(parentPath, slug, new File(fsPath))
      mf._sha1 = sha1
      mf._meta = MediaFileMeta(
        title = title,
        artist = Option(artist).filter({ a => !a.isEmpty }),
        album = album,
        track = track,
        year = year,
        durationSeconds = durationSeconds
      )
      mf
    }).toList
  }

  def listQueues(): String = {
    import scala.collection.JavaConversions._
    val conn = connectionBuilder.connect()
    val result: Cursor[util.HashMap[String, _]] = r.db("music").table("queues").run(conn)
    val queues: List[JsValue] = result.iterator().map({ item =>
      JsString(item.get("id").toString)
    }).toList.sortBy(_.value)
    val json = JsArray(queues)
    Json.stringify(json)
  }

  def saveQueue(name: String, songs: List[QueueItem]): Unit = {
    val original: Option[PlayQueue] = getQueue(name)
    println("got original queue")
    val (index, seconds): (Int, Double) = original.map({ q =>
      val idx = q.song
      if (songs.size > idx && q.queue.size > idx && songs(idx).path == q.queue(idx).path) {
        (idx, q.seconds)
      } else {
        val maybe: Option[(QueueItem, Int)] = songs.zipWithIndex.find(q.queue.size > idx && _._1.path == q.queue(idx).path)
        val newIndex = maybe.map(_._2).getOrElse(0)
        val seconds: Double = if (songs.size > newIndex && q.queue.size > idx && songs(newIndex).path == q.queue(idx).path) {
          q.seconds
        } else {
          0
        }
        (newIndex, seconds)
      }
    }).getOrElse((0, 0.0))
    val queue = new PlayQueue(name)
    queue.queue = songs
    queue.seconds = seconds
    queue.song = index
    original.foreach(pq => {
      queue.mapFrom(pq.queue)
    })
    savePlayQueue(queue)
  }

  def savePosition(name: String, song: Int, seconds: Double): Unit = {
    getQueue(name).foreach(queue => {
      val conn = connectionBuilder.connect()
      queue.song = song
      queue.seconds = seconds
      queue.queue(song).secondsPosition = seconds.toInt
      savePlayQueue(queue)
    })
  }

  def getQueue(name: String): Option[PlayQueue] = {
    getQueueJson(name).flatMap(PlayQueue.fromJson(_, this))
  }

  def savePlayQueue(queue: PlayQueue): Unit = {
    val conn = connectionBuilder.connect()
    r.db("music").table("queues").insert(
      r.hashMap("id", queue.id)
        .`with`("song", queue.song)
        .`with`("seconds", queue.seconds)
        .`with`("queue", queue.queue.map({qi =>
          r.hashMap("path", qi.path)
            .`with`("secondsDuration", qi.secondsDuration)
            .`with`("title", qi.title)
            .`with`("artist", qi.artist.getOrElse(""))
            .`with`("album", qi.album)
            .`with`("track", qi.track)
            .`with`("year", qi.year)
            .`with`("secondsPosition", qi.secondsPosition)
            .`with`("sha1", qi.sha1)
        }).toArray)
    )
      .optArg("conflict", "update")
      .run(conn)
    conn.close()
  }

  def deleteQueue(name: String): Unit = {
    val conn = connectionBuilder.connect()
    r.db("music").table("queues").filter(
      r.hashMap("id", name)
    )
      .delete()
      .run(conn)
    conn.close()
  }

  def getAgentUsingQueue(queue: String): List[Agent] = {
    import scala.collection.JavaConversions._
    val conn = connectionBuilder.connect()
    val result: Cursor[util.HashMap[String, _]] = r.db("music").table("agents").filter(
      r.hashMap("queue", queue)
    ).run(conn)
    println(s"Result size: ${result.bufferedSize()}")
    result.iterator().flatMap({ r =>
      val n = r.get("id")
      n match {
        case name: String =>
          val q = r.get("queue") match {
            case qs: String => { Option(qs) }
            case _ => { None }
          }
          Option(new Agent(name, q))
        case _ =>
          None
      }
    }).toList
  }

  def removeQueueFromAgent(agent: Agent): Unit = {
    val conn = connectionBuilder.connect()
    r.db("music").table("agents").get(agent.name)
      .replace(new ReqlFunction1 {
        override def apply(arg1: ReqlExpr): AnyRef = {
          arg1.without("queue")
        }
      }).run(conn)
  }

  def getQueueJson(name: String): Option[JsValue] = {
    import scala.collection.JavaConversions._
    val conn = connectionBuilder.connect()
    val result: util.HashMap[String, _] = r.db("music").table("queues").get(name).run(conn)
    val ret = if (result != null) {
      val json = JsObject(result.map({
        case (key: String, value: String) => {
          key -> JsString(value)
        }
        case (key: String, value: util.ArrayList[util.HashMap[String, _]]) if key == "queue" => {
          val queue: List[JsObject] = value.map({ js: util.HashMap[String, _] =>
            val seconds = js.get("secondsDuration") match {
              case s: Double => { s }
              case s: Long => { s.toDouble }
              case _ => { 0.0 }
            }
            JsObject(List(
              "path" -> JsString(js.get("path").asInstanceOf[String]),
              "album" -> JsString(js.get("album").asInstanceOf[String]),
              "artist" -> JsString(js.get("artist").asInstanceOf[String]),
              "secondsDuration" -> JsNumber(BigDecimal(seconds)),
              "title" -> JsString(js.get("title").asInstanceOf[String]),
              "track" -> JsNumber(js.get("track").asInstanceOf[Long]),
              "year" -> JsNumber(js.get("year").asInstanceOf[Long]),
              "secondsPosition" -> JsNumber(js.get("secondsPosition").asInstanceOf[Long]),
              "sha1" -> JsString(js.get("sha1").asInstanceOf[String])
            ))
          }).toList
          key -> JsArray(queue)
        }
        case (key: String, value: Long) => {
          key -> JsNumber(value)
        }
        case (key: String, value: Double) => {
          key -> JsNumber(value)
        }
      }))
      Some(json)
    } else {
      None
    }
    conn.close()
    ret
  }

  def getQueueString(name: String): Option[String] = {
    getQueueJson(name).map(Json.stringify)
  }

  def getAgent(name: String): Option[Agent] = {
    println(s"Looking for agent ${name}")
    val conn = connectionBuilder.connect()
    val result: util.HashMap[String, _] = r.db("music").table("agents").get(name).run(conn)
    val ret = Option(result).map({ r =>
      val name: String = r.get("id").asInstanceOf[String]
      val queue: Option[String] = Option(r.get("queue").asInstanceOf[String])
      new Agent(name, queue)
    })
    conn.close()
    if (ret.isDefined) {
      println("Found an agent to return")
    } else {
      println("No agent to return")
    }
    ret
  }

  def saveAgent(agent: Agent): Unit = {
    val conn = connectionBuilder.connect()
    r.db("music").table("agents").insert(
      r.hashMap("id", agent.name)
        .`with`("queue", agent.queueName.orNull)
    )
      .optArg("conflict", "update")
      .run(conn)
    conn.close()
  }

  def getAuthApprovedByToken(token: String): Option[AgentAuth] = {
    println(s"Looking for token ${token}")
    val conn = connectionBuilder.connect()
    try {
      val result: Cursor[util.HashMap[String, _]] = r.db("music").table("auth").filter(
        r.hashMap("token", token)
          .`with`("status", AgentAuth.Status.toString(AgentAuth.Status.Approved))
      ).run(conn)
      val ret = resultToAgentAuth(Option(result.next()))
      ret
    } catch {
      case e: NoSuchElementException => { None }
    } finally {
      conn.close()
    }
  }

  def resultToAgentAuth(result: Option[util.HashMap[String, _]]): Option[AgentAuth] = {
    result.flatMap({ r =>
      val name: String = r.get("id").asInstanceOf[String]
      val token: Option[String] = Option(r.get("token").asInstanceOf[String])
      val status: Option[AgentAuth.Status.Status] = Option(r.get("status").asInstanceOf[String]).flatMap({ s => AgentAuth.Status.fromString(s) })
      val lastLogin: Option[String] = Option(r.get("lastLogin").asInstanceOf[String])
      for {
        _token <- token
        _status <- status
        _lastLogin <- lastLogin
      } yield new AgentAuth(name, _token, _status, _lastLogin)
    })
  }

  def register(name: String): Option[AgentAuth] = {
    def getAuthByName(name: String): Option[AgentAuth] = {
      val conn = connectionBuilder.connect()
      val result: util.HashMap[String, _] = r.db("music").table("auth").get(name).run(conn)
      val ret = resultToAgentAuth(Option(result))
      conn.close()
      ret
    }

    if (getAuthByName(name).isEmpty) {
      val token = generateToken()
      val auth = new AgentAuth(name, token, AgentAuth.Status.Pending, "Last Updated Not Yet Implemented")
      val conn = connectionBuilder.connect()
      r.db("music").table("auth").insert(
        r.hashMap("id", auth.name)
          .`with`("token", auth.token)
          .`with`("status", AgentAuth.Status.toString(auth.status))
          .`with`("lastLogin", auth.lastLogin)
      )
        .run(conn)
      conn.close()
      Option(auth)
    } else {
      None
    }
  }

  def generateToken(): String = {
    val keySource: String = "random"
    val bytes = new Array[Byte](50)
    scala.util.Random.nextBytes(bytes)
    Base64.getEncoder.encodeToString(bytes)
  }

  class QueueWatcher(name: String, callback: () => Unit) {
    private val conn = connectionBuilder.connect()
    private val result: Cursor[java.util.HashMap[String, _]] = r.db("music").table("queues").get(name).changes().run(conn)

    new Thread(new Runnable {
      override def run(): Unit = {
        var next: java.util.HashMap[String, _] = null
        while ({next = result.next(); next} != null) {
          println(s"Change Detected: ${next.size()}")
          callback()
        }
        println(s"Done listening to ${name}")
      }
    }).start()

    def close(): Unit = {
      result.close()
    }

    def isOpen: Boolean = {
      conn.isOpen
    }
  }

  def sha1(input: String): String = {
    val md = MessageDigest.getInstance("SHA-1")
    md.digest(input.getBytes).map("%02x".format(_)).mkString
  }
}
