package service

import javax.inject.Singleton

import controllers.AgentWebSocketActor
import controllers.InterfaceWebSocketActor
import models.Agent
import play.api.libs.json.JsArray
import play.api.libs.json.JsBoolean
import play.api.libs.json.JsNumber
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue

@Singleton
class InterfaceService {
  var interfaces: List[InterfaceWebSocketActor] = List()

  def add(interface: InterfaceWebSocketActor): Unit = {
    interfaces = interface :: interfaces
  }

  def remove(interface: InterfaceWebSocketActor): Unit = {
    interfaces = interfaces.filter({ _ != interface })
  }
}

object InterfaceService {
  def COMMAND_AGENTS_CHANGE(added: List[AgentWebSocketActor], removed: List[AgentWebSocketActor], actual: List[AgentWebSocketActor]): JsValue = {
    JsObject(List(
      "command" -> JsString("agents_change"),
      "added_agents" -> JsArray(added.flatMap({a => a.state.map({state => JsString(state.name)} )})),
      "removed_agents" -> JsArray(removed.flatMap({a => a.state.map({state => JsString(state.name)} )})),
      "agents" -> JsArray(actual.map({a => JsString(a.state.get.name)}))
    ))
  }
  def COMMAND_FULL_QUEUE(agent: Agent, database: DatabaseService): Option[JsValue] = {
    agent.queueName.flatMap(database.getQueueJson(_).map({ q =>
      JsObject(List(
        "command" -> JsString("queue"),
        "queue" -> q
      ))
    }))
  }
  def COMMAND_AGENT_UPDATE_POSITION(agent: Agent, seconds: Double, song: Int): JsValue = {
    JsObject(List(
      "command" -> JsString("agent_update"),
      "seconds" -> JsNumber(seconds),
      "song" -> JsNumber(song)
    ))
  }
  def COMMAND_AGENT_UPDATE_STATE(agent: Agent): JsValue = {
    JsObject(List(
      "command" -> JsString("update_state"),
      "playState" -> JsString(agent.printablePlayState),
      "seconds" -> JsNumber(agent.playPositionSeconds),
      "cache" -> JsArray(agent.cacheState.map({ case (slug, state) =>
          JsObject(List(
            "slug" -> JsString(slug),
            "state" -> JsNumber(state)
          ))
      }))
    ))
  }
  def COMMAND_AGENT_UPDATE_DISCONNECT(agent: Agent): JsValue = {
    JsObject(List(
      "command" -> JsString("agent_update"),
      "disconnected" -> JsBoolean(true)
    ))
  }
  def COMMAND_INFO(msg: String): JsValue = {
    JsObject(List("command" -> JsString("info"), "message" -> JsString(msg)))
  }
  def COMMAND_ERROR(msg: String): JsValue = {
    JsObject(List("command" -> JsString("error"), "message" -> JsString(msg)))
  }
}
