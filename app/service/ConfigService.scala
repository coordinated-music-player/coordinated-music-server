package service

import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

import models.ConfigPath
import models.MediaFile
import models.MediaFolder
import play.api.Configuration
import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import play.api.libs.json.JsString

import scala.collection.JavaConversions._

@Singleton
class ConfigService @Inject() (configuration: Configuration) {
  def rethinkIp: String = {
    configuration.getString("media.rethinkIp").get
  }

  val extensions: Seq[String] = configuration.getList("media.extensions") match {
    case Some(lst) => lst.map({ _.unwrapped().toString })
    case None => Nil
  }

  def getPaths: List[MediaFolder] = {
    configuration.getConfigList("media.paths") match {
      case Some(lst) => {
        val arr = lst.toList
        arr.map({ obj: Configuration =>
          val cfg = ConfigPath(obj.getString("label").get, obj.getString("path").get)
          new MediaFolder("", cfg.label, new File(cfg.path), extensions)
        })
      }
      case None => {
        Nil
      }
    }
  }

  def getPathsAsJsonString(depth: Int, db: DatabaseService): String = {
    JsObject(List(
      "path" -> JsString("_"),
      "children" -> JsArray(getPaths.map(_.toJson(depth - 1, db)))
    )).toString()
  }

  def findMediaFile(path: String): Option[MediaFile] = {
    val media = getPaths
    val pathComponents = path.split("/").toList.filter(!_.isEmpty)
    media.foldLeft[Option[MediaFile]](None)({ (found, next) =>
      Option(found.getOrElse[MediaFile]({
        if (next.name.equalsIgnoreCase(pathComponents.head)) {
          next.find(pathComponents.tail).orNull
        } else {
          null
        }
      }))
    })
  }
}
