package controllers

import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import javax.inject.Inject

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.PoisonPill
import akka.actor.Props
import akka.stream.Materializer
import models.Agent
import models.AgentAuth
import play.api.Configuration
import play.api.libs.json.JsArray
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.streams.ActorFlow
import play.api.libs.ws.WSClient
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.mvc.Result
import play.api.mvc.WebSocket
import service.AgentService
import service.DatabaseService

class AgentController @Inject() (database: DatabaseService,
                                 agents: AgentService,
                                 configuration: Configuration,
                                 ws: WSClient,
                                 implicit val system: ActorSystem,
                                 implicit val mat: Materializer
                                ) extends Controller {
  def connectSocket(token: String) = WebSocket.accept[String, String] { request =>
    println("Request for agent socket received")
    database.getAuthApprovedByToken(token) match {
      case Some(auth) => {
        println(s"Found the token, returning the connection for ${auth.name}")
        ActorFlow.actorRef(out => AgentWebSocketActor.props(agents, auth, out, database))
      }
      case None => {
        println("Token not found, refuse connection")
        ActorFlow.actorRef(out => RejectingWebSocketActor.props(out))
      }
    }
  }

  def list() = Action {
    val jsonAgents: JsArray = JsArray(agents.agents.filter({ agent =>
      agent.state.isDefined
    }).map({ agent =>
      JsString(agent.state.get.name)
    }))
    Ok(jsonAgents)
  }

  def play(name: String) = Action {
    actOnAgent(name) { agent =>
      agent.commandPlay
      Ok(s"play command sent to ${name}\n")
    }
  }

  def next(name: String) = Action {
    actOnAgent(name) { agent =>
      agent.commandNext
      Ok(s"next command sent to ${name}\n")
    }
  }

  def prev(name: String) = Action {
    actOnAgent(name) { agent =>
      agent.commandPrev
      Ok(s"prev command sent to ${name}\n")
    }
  }

  def jump(name: String, songPosition: Int) = Action {
    actOnAgent(name) { agent =>
      agent.commandJump(songPosition)
      Ok(s"jump command sent to ${name}, position ${songPosition}\n")
    }
  }

  def pause(name: String) = Action {
    actOnAgent(name) { agent =>
      agent.commandPause
      Ok(s"pause command sent to ${name}\n")
    }
  }

  def stop(name: String) = Action {
    actOnAgent(name) { agent =>
      agent.commandStop
      Ok(s"stop command sent to ${name}\n")
    }
  }

  def seek(name: String, seconds: Double) = Action {
    actOnAgent(name) { agent =>
      agent.commandSeek(seconds)
      Ok(s"Told ${name} to seek to ${seconds} seconds\n")
    }
  }

  def queue(name: String, queueName: String) = Action {
    actOnAgent(name) { agent =>
      val owningAgents = database.getAgentUsingQueue(queueName)
      owningAgents.foreach({ rawAgent =>
        println(s"Agent: ${rawAgent.name}")
        agents.find(rawAgent.name) match {
          case Some(agentSock) => {
            println("The agent is connected, so let's have it drop the queue")
            agentSock.commandDropQueueAndWait
          }
          case None => {
            println("The agent is not connected, so we can just take the queue from it")
            database.removeQueueFromAgent(rawAgent)
          }
        }
      })
      agent.commandFullQueue(queueName)
      Ok(s"Playlist for ${name} set to ${queueName}\n")
    }
  }

  def actOnAgent(name: String)(fun: AgentWebSocketActor => Result): Result = {
    agents.find(name) match {
      case Some(agent) => {
        fun(agent)
      }
      case _ => {
        BadRequest(s"Agent not found: ${name}\n")
      }
    }
  }
}

class RejectingWebSocketActor(out: ActorRef) extends Actor {
  out ! Json.stringify(AgentService.COMMAND_ERROR("Access Denied"))
  out ! PoisonPill
  println("Opening temporary socket to inform rejection")

  override def receive = {
    case msg: String => {
      println(s"Received a message from a websocket that we should be rejecting: ${msg}")
    }
    case _ => {}
  }

  override def postStop(): Unit = {
    println(s"Temporary socket closed")
  }
}

object RejectingWebSocketActor {
  def props(out: ActorRef): Props = {
    Props(new RejectingWebSocketActor(out))
  }
}

class AgentWebSocketActor(agents: AgentService, auth: AgentAuth, out: ActorRef, database: DatabaseService) extends Actor {
  var state: Option[Agent] = database.getAgent(auth.name)
  var listeners: List[InterfaceWebSocketActor] = Nil
  private var queueWatcher: Option[database.QueueWatcher] = None
  var hasQueue: CountDownLatch = new CountDownLatch(0)
  state.foreach({ agent =>
    if (agent.queueName.isDefined) {
      AgentService.COMMAND_FULL_QUEUE(agent, database).foreach(out ! Json.stringify(_))
    }
  })
  agents.add(AgentWebSocketActor.this)
  updateQueueWatcher()

  def addListener(interface: InterfaceWebSocketActor): Unit = {
    listeners = interface :: listeners
    state.foreach({ agent =>
      interface.sendFullQueue(agent)
      interface.notifyAgentPlayStateChange(agent)
    })
  }

  def removeListener(interface: InterfaceWebSocketActor): Unit = {
    listeners = listeners.filter(_ != interface)
  }

  def updateQueueWatcher(): Unit = {
    queueWatcher.foreach(_.close())
    queueWatcher = None
    (state, state.flatMap(_.queueName)) match {
      case (Some(agent), Some(queueName)) => {
        queueWatcher = Option(new database.QueueWatcher(queueName, {() =>
          println(s"The queue ${queueName} has been updated.")
          AgentService.COMMAND_FULL_QUEUE(agent, database).foreach(out ! Json.stringify(_))
          listeners.foreach(_.sendFullQueue(agent))
        }))
      }
      case _ => {
        println("Can't update the agent either because there is no agent, or because there is no queue")
        listeners.foreach(_.sendInfo(s"Cannot update queue.  Either there is no agent, or there is no queue: ${state}, ${state.flatMap(_.queueName)}"))
      }
    }
  }

  override def receive = {
    case msg: String => {
      println(s"Agent Message: ${msg}")
      val json: JsValue = Json.parse(msg)
      (json \ "command").asOpt[String] match {
        case Some("position") => {
          ((json \ "queue").asOpt[String], (json \ "seconds").asOpt[Double], (json \ "song").asOpt[Int]) match {
            case (Some(queue), Some(seconds), Some(song)) if state.isDefined => {
              println(s"We should save the position to couch: ${seconds}, ${song}")
              database.savePosition(queue, song, seconds)
              out ! Json.stringify(AgentService.COMMAND_INFO("Queue position saved"))
              if (state.flatMap(_.queueName).contains(queue)) {
                listeners.foreach(_.notifyPositionChanged(state.get, seconds, song))
              }
            }
            case _ if state.isEmpty => {
              println("Agent sent play position but is not registered")
              // TODO: send a registration request
            }
            case _ => {
              val err = s"Malformed command, missing seconds or song or queue name: ${msg}"
              println(err)
              out ! Json.stringify(AgentService.COMMAND_ERROR(err))
            }
          }
        }
        case Some("update_state") => {
          (json \ "playState").asOpt[String] match {
            case Some(newState) => {
              state.foreach({ s =>
                s.setPlayStateFromString(newState)
                val seconds: Double = (json \ "seconds").asOpt[Double].getOrElse(0)
                s.playPositionSeconds = seconds
                val cacheState = (json \ "cache").asOpt[JsArray].getOrElse(JsArray()).value.map({ js: JsValue =>
                  val slug = (js \ "slug").asOpt[String].get
                  val state = (js \ "state").asOpt[Double].getOrElse(0.0)
                  (slug, state)
                })
                s.cacheState = cacheState.toList
                listeners.foreach(_.notifyAgentPlayStateChange(s))
              })
            }
          }
        }
        case Some("queue_dropped") => {
          database.removeQueueFromAgent(state.get)
          hasQueue.countDown()
        }
        case Some(cmd) => {
          println(s"Received unknown command ${cmd}")
          out ! Json.stringify(AgentService.COMMAND_ERROR(s"Unrecognized Command: ${cmd}"))
        }
        case _ => {
          println(s"Received message with no command: ${msg}")
          out ! Json.stringify(AgentService.COMMAND_ERROR("No command received"))
        }
      }
    }
    case _ => {
      println("Unknown message")
    }
  }

  def commandPlay: Unit = {
    out ! Json.stringify(AgentService.COMMAND_PLAY)
  }

  def commandNext: Unit = {
    out ! Json.stringify(AgentService.COMMAND_NEXT)
  }

  def commandPrev: Unit = {
    out ! Json.stringify(AgentService.COMMAND_PREV)
  }

  def commandJump(songPosition: Int): Unit = {
    out ! Json.stringify(AgentService.COMMAND_JUMP(songPosition))
  }

  def commandPause: Unit = {
    out ! Json.stringify(AgentService.COMMAND_PAUSE)
  }

  def commandStop: Unit = {
    out ! Json.stringify(AgentService.COMMAND_STOP)
  }

  def commandSeek(seconds: Double): Unit = {
    out ! Json.stringify(AgentService.COMMAND_SEEK(seconds))
  }

  def commandFullQueue(queueName: String): Unit = {
    state match {
      case Some(agent) => {
        agent.setQueueName(queueName)
        database.saveAgent(agent)
        AgentService.COMMAND_FULL_QUEUE(agent, database).foreach(out ! Json.stringify(_))
        listeners.foreach(_.sendFullQueue(agent))
        updateQueueWatcher()
      }
      case None => {
        println("Cannot set playlist when the agent has not registered")
      }
    }
  }

  def commandDropQueueAndWait: Unit = {
    state.foreach({ s =>
      if (s.queueName.isDefined) {
        hasQueue = new CountDownLatch(1)
        commandDropQueue
        hasQueue.await(5, TimeUnit.SECONDS)
      }
    })
  }

  def commandDropQueue: Unit = {
    out ! Json.stringify(AgentService.COMMAND_DROP_QUEUE)
  }

  override def postStop(): Unit = {
    agents.remove(this)
    queueWatcher.foreach(_.close())
    queueWatcher = None
    println(s"Socket is closed.  There are now ${agents.agents.length} agents connected")
  }

  def kill(): Unit = {
    out ! Json.stringify(AgentService.COMMAND_GIVE_UP)
    out ! PoisonPill
  }
}

object AgentWebSocketActor {
  def props(agents: AgentService, auth: AgentAuth, out: ActorRef, database: DatabaseService): Props = {
    Props(new AgentWebSocketActor(agents, auth, out, database))
  }
}
