package controllers

import java.net.URLDecoder
import javax.inject.Inject

import akka.actor.ActorSystem
import akka.stream.Materializer
import models.AgentAuth
import models.MediaFile
import models.QueueItem
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import play.api.libs.ws.WSClient
import play.api.mvc.Action
import play.api.mvc.AnyContent
import play.api.mvc.Controller
import play.api.mvc.Request
import play.api.mvc.Result
import service.AgentService
import service.ConfigService
import service.DatabaseService

class ApiController @Inject() (database: DatabaseService,
                               agents: AgentService,
                               configuration: ConfigService,
                               ws: WSClient,
                               implicit val system: ActorSystem,
                               implicit val mat: Materializer
                              ) extends Controller {
  def listMedia(depth: Option[Int]) = Action {
    Ok(configuration.getPathsAsJsonString(depth.getOrElse(1), database))
  }

  def list(pathRaw: String, depth: Option[Int]) = Action {
    val path = URLDecoder.decode(pathRaw, "UTF-8")
    val pathComponents = path.split("/")
    val roots = configuration.getPaths
    roots.flatMap({ root =>
      root.findFolder(pathComponents).map({ folder =>
        Ok(folder.toJson(depth.getOrElse(1), database).toString())
      })
    }).headOption.getOrElse(NotFound("{status: \"Stuff not found\"}"))
  }

  def index(pathRaw: String, depth: Option[Int]) = Action {
    val path = URLDecoder.decode(pathRaw, "UTF-8")
    println(s"Indexing ${path}")
    val pathComponents = path.split("/")
    val roots = configuration.getPaths
    roots.foreach({ root =>
      root.findFolder(pathComponents).foreach({ folder =>
        folder.index(depth.getOrElse(1), database)
      })
    })
    Ok("{status: \"done\"}")
  }

  def fetch(pathRaw: String) = Action { request: Request[AnyContent] =>
    val path = URLDecoder.decode(pathRaw, "UTF-8")
    requireAuthentication(request.headers.get("token"))({ agent =>
      println(s"We should fetch the file ${path}")
      val media: Option[MediaFile] = database.getDeepMedia(path).headOption
      media match {
        case Some(m) => {
          Ok.sendFile(m.path)
        }
        case None => {
          NotFound("")
        }
      }
    })
  }

  def listQueues() = Action {
    Ok(database.listQueues())
  }

  def getQueue(name: String) = Action {
    // curl -H "Content-Type: application/json" -X GET http://localhost:9000/api/queue/doug
    database.getQueueString(name) match {
      case Some(result) => Ok(result)
      case None => NotFound("Item Not Found")
    }
  }

  def deleteQueue(queueNameRaw: String) = Action {
    // curl -H "Content-Type: application/json" -X DELETE http://localhost:9000/api/queue/doug
    val queueName = URLDecoder.decode(queueNameRaw, "UTF-8")
    println("Try to delete the queue")
    val owningAgents = database.getAgentUsingQueue(queueName)
    owningAgents.foreach({ rawAgent =>
      println(s"Agent: ${rawAgent.name}")
      agents.find(rawAgent.name) match {
        case Some(agentSock) => {
          println("The agent is connected, so let's have it drop the queue")
          agentSock.commandDropQueueAndWait
        }
        case None => {
          println("The agent is not connected, so we can just take the queue from it")
          database.removeQueueFromAgent(rawAgent)
        }
      }
    })
    database.deleteQueue(queueName)
    Ok("Done")
  }

  def savePosition(name: String, song: Int, seconds: Double) = Action { request: Request[AnyContent] =>
    // curl -H "Content-Type: application/json" -X PUT http://localhost:9000/api/queue/doug/1/2 -d '[]'
    database.savePosition(name, song, seconds)
    Ok("Done")
  }

  def saveQueue(nameRaw: String) = Action { request: Request[AnyContent] =>
    // curl -H "Content-Type: application/json" -X PUT http://localhost:9000/api/queue/doug -d '["01.mp3", "02.mp3", "03.mp3"]'
    val name = URLDecoder.decode(nameRaw, "UTF-8")
    val body: Option[JsValue] = request.body.asJson
    body match {
      case Some(playlist) => {
        val queue = playlist.asOpt[List[JsValue]].getOrElse(Nil)
          .flatMap(jsv => jsv.asOpt[String])
          .flatMap(slug => QueueItem.create(slug, configuration))
        database.saveQueue(name, queue)
        Ok(s"Name is: ${name}\n")
      }
      case _ => {
        BadRequest("Must submit a playlist as the body of the request\n")
      }
    }
  }

  def replaceQueue(path: String, name: String) = Action { request: Request[AnyContent] =>
    println(s"appendQueue/${path}, ${name}")
    val playlist: List[QueueItem] = database.getDeepMedia(URLDecoder.decode(path, "UTF-8")).map({ mf => QueueItem.fromMediaFile(mf) })
    database.saveQueue(name, playlist)
    Ok(s"Name is: ${name}\n")
  }

  def appendQueue(path: String, name: String) = Action { request: Request[AnyContent] =>
    println(s"appendQueue/${path}, ${name}")
    val playlist: List[QueueItem] = database.getDeepMedia(URLDecoder.decode(path, "UTF-8")).map({ mf => QueueItem.fromMediaFile(mf) })
    val old: List[QueueItem] = database.getQueueJson(name) match {
      case Some(queue) => {
        queue.\("queue").asOpt[List[JsObject]] match {
          case Some(q) => { q.flatMap({ js => QueueItem.fromJson(js, database) }) }
          case None => { List() }
        }
      }
      case None => { List() }
    }
    val combined: List[QueueItem] = old ++ playlist
    database.saveQueue(name, combined)
    Ok(s"Name is: ${name}\n")
  }

  def requireAuthentication(agentToken: Option[String])(f: AgentAuth => Result): Result = {
    println(s"Checking authentication for '${agentToken.getOrElse("no token")}'")
    agentToken.flatMap({ at => database.getAuthApprovedByToken(at) }) match {
      case Some(token) => f(token)
      case None => Unauthorized
    }
  }
}
