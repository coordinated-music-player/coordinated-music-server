package controllers

import javax.inject.Inject

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.stream.Materializer
import models.Agent
import models.Agent.PlayState.PlayState
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.streams.ActorFlow
import play.api.libs.ws.WSClient
import play.api.mvc.Controller
import play.api.mvc.WebSocket
import service.AgentService
import service.ConfigService
import service.DatabaseService
import service.InterfaceService

class InterfaceController @Inject() (database: DatabaseService,
                                     configuration: ConfigService,
                                     ws: WSClient,
                                     interfaces: InterfaceService,
                                     agents: AgentService,
                                     implicit val system: ActorSystem,
                                     implicit val mat: Materializer
                                    ) extends Controller {
  def connectSocket = WebSocket.accept[String, String] { request =>
    println("Request for socket received")
    ActorFlow.actorRef(out => InterfaceWebSocketActor.props(interfaces, agents, out, database))
  }
}

class InterfaceWebSocketActor(interfaces: InterfaceService, agents: AgentService, out: ActorRef, database: DatabaseService) extends Actor {
  interfaces.add(this)
  out ! Json.stringify(InterfaceService.COMMAND_INFO("Welcome Aboard"))
  sendAgentsUpdate(Nil, Nil, agents.agents)

  var agentListen: Option[AgentWebSocketActor] = None

  override def receive = {
    case msg: String => {
      println(s"Interface Message: ${msg}")
      val json: JsValue = Json.parse(msg)
      (json \ "command").asOpt[String] match {
        case Some("listen") => {
          // start listening to the given agent
          val agent: Option[AgentWebSocketActor] = (json \ "agent").asOpt[String].flatMap({ agentName =>
            agents.find(agentName)
          })
          agentListen.foreach(_.removeListener(this))
          agentListen = agent
          agentListen.foreach(_.addListener(this))
        }
        case _ => {
          println(s"Unrecognized Command: ${msg}")
        }
      }
    }
  }

  override def postStop(): Unit = {
    interfaces.remove(this)
  }

  def sendAgentsUpdate(added: List[AgentWebSocketActor], removed: List[AgentWebSocketActor], actual: List[AgentWebSocketActor]): Unit = {
    out ! Json.stringify(InterfaceService.COMMAND_AGENTS_CHANGE(added, removed, actual))
  }

  def sendFullQueue(agent: Agent): Unit = {
    InterfaceService.COMMAND_FULL_QUEUE(agent, database).foreach(out ! Json.stringify(_))
  }

  def notifyPositionChanged(agent: Agent, seconds: Double, song: Int): Unit = {
    out ! Json.stringify(InterfaceService.COMMAND_AGENT_UPDATE_POSITION(agent, seconds, song))
  }

  def notifyAgentDisconnect(agent: Agent): Unit = {
    out ! Json.stringify(InterfaceService.COMMAND_AGENT_UPDATE_DISCONNECT(agent))
    agentListen = None
  }

  def notifyAgentPlayStateChange(agent: Agent): Unit = {
    out ! Json.stringify(InterfaceService.COMMAND_AGENT_UPDATE_STATE(agent))
  }

  def sendInfo(msg: String): Unit = {
    out ! Json.stringify(InterfaceService.COMMAND_INFO(msg))
  }
}

object InterfaceWebSocketActor {
  def props(interfaces: InterfaceService, agents: AgentService, out: ActorRef, database: DatabaseService): Props = {
    Props(new InterfaceWebSocketActor(interfaces, agents, out, database))
  }
}
