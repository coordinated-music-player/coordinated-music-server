package controllers

import java.net.URLDecoder
import javax.inject.Inject

import models.Agent
import play.api.mvc.Action
import play.api.mvc.Controller
import service.DatabaseService

class AuthController @Inject() (database: DatabaseService) extends Controller {
  def register(rawName: String) = Action {
    val name = URLDecoder.decode(rawName, "UTF-8")
    println(s"Received registration request from ${name}")
    database.register(name) match {
      case Some(auth) => {
        database.saveAgent(new Agent(auth.name, None))
        Ok(auth.toFullJson())
      }
      case None => {
        Unauthorized
      }
    }
  }
}
